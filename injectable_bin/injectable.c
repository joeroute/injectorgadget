#include <stdio.h>
#include <windows.h>
#include <processthreadsapi.h>

int main(void)
{
    printf("PID: %d", GetCurrentProcessId());
    
    //Run forever in infinite loop until killed. Sleep so CPU doesn't get pegged and so thread is alertable
    while(1)
    {
       SleepEx(300, TRUE);
    }

    return 0;
}