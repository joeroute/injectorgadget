#include <windows.h>
#include <stdio.h>

typedef VOID (*sc_f)();
PVOID readShellcode(char *filename, PDWORD filesize);


int main(int argc, char *argv[])
{
    DWORD filesize = 0;
    DWORD oldprotect = 0;
    sc_f sc = NULL;
    if (argc != 2)
    {
        printf("Usage: %s [sc_file]\n", argv[0]);
        return -1;
    }

    sc = (sc_f) readShellcode(argv[1], &filesize);
    if (NULL == sc)
    {
        printf("Failed to read in shellcode\n");
        return -1;
    }
    printf("attmpting to execute %s size: %d", argv[1], filesize);
    
    //Set perms to exectuable
    VirtualProtect((LPVOID)sc, (SIZE_T)filesize, PAGE_EXECUTE_READWRITE, &oldprotect);
    //Run the shellcode
    sc();
    
    HeapFree(GetProcessHeap(), 0, (LPVOID)sc);
    return 0;
}

PVOID readShellcode(char *filename, PDWORD filesize)
{
    DWORD64 retval = 0;
    DWORD highbyte = 0;
    LPVOID sc_mem = NULL;
    HANDLE hScFile = CreateFileA(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (INVALID_HANDLE_VALUE == hScFile)
    {
        goto error;
    }

    *filesize = GetFileSize(hScFile, &highbyte);
    
    if (!(sc_mem = HeapAlloc(GetProcessHeap(),  0, (SIZE_T)(*filesize))))
    {
        goto error;
    }
    if(!ReadFile(hScFile, (LPVOID)sc_mem, *filesize, &highbyte, NULL))
    {
        HeapFree(GetProcessHeap(), 0, sc_mem);
        sc_mem = NULL;
    }

    return sc_mem;

error:
    if (NULL != sc_mem)
    {
        HeapFree(GetProcessHeap(), 0 , sc_mem);
    }
    return NULL;    

}
