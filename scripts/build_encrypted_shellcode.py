import struct
import argparse
import random
"""
Simple python program to concatenate encrypted shellcode with the xor_stub
Note: Meant to be run from the root directory of the project.
"""


def place_stub(enc_shellcode, xor_key):
    stub_sc = bytearray()
    #Args for top of shellcode
    sc_args = struct.pack("<I", len(enc_shellcode))
    sc_args += xor_key
    with open("xor_stub/xor_decrypt_sc", "rb") as stub_file:
        stub_sc = stub_file.read()

    return b"".join([stub_sc, sc_args, enc_shellcode])

def encrypt_shellcode(shellcode, xor_key):
    enc_sc = bytearray()
    for item in enumerate(shellcode, 0):
        enc_sc += (item[1] ^ xor_key[item[0]]).to_bytes(1, 'big')
    return enc_sc


if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("-f", "--file", help="Path to shellcode file", required=True)
    argparser.add_argument("-o", "--output", help="Output of encrypted shellcode", required=True)
    args = argparser.parse_args()

    dec_sc = bytearray()
  
    with open(args.file, "rb") as sc_file:
        dec_sc += sc_file.read()
    
    #generate random key
    xor_key = random.randbytes(8)
    enc_sc = encrypt_shellcode(dec_sc, xor_key)
    full_sc = place_stub(enc_sc, xor_key)

    with open(args.output, "wb") as out_file:
        out_file.write(full_sc)



