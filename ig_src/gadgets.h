#ifndef GADGETS_H
#define GADGETS_H
#include "InjectorGadget.h"

IGSTATUS ig_CreateThread(PINJECTOR_GADGET ig);
IGSTATUS ig_CreateRemoteThread(PINJECTOR_GADGET ig);
IGSTATUS ig_QueueUserApc(PINJECTOR_GADGET ig);
IGSTATUS ig_SetThreadContext(PINJECTOR_GADGET ig);

#endif //GADGETS_H
