#ifndef INJECTOR_GADGET_H
#define INJECTOR_GADGET_H
#include <windows.h>

typedef struct INJECTOR_GADGET INJECTOR_GADGET, *PINJECTOR_GADGET;

typedef enum
{
    IG_SUCCESS,
    IG_GENERIC_ERROR,
    IG_INVALID_ARGS,
    IG_INVALID_HANDLE,
    IG_EXECUTE_ERROR,
    IG_ALLOC_ERROR,
    IG_WRITE_ERROR,
    IG_PROTECT_ERROR,
    IG_FAILED_THREAD_SUSPEND,
    IG_FAILED_THREAD_CTX,
    IG_FAILED_THREAD_RESUME
}IGSTATUS;

typedef enum InjectionMethod
{
    LOCAL_SHELLCODE,	
    CREATE_REMOTE_THREAD,
    QUEUE_USER_APC,
    SET_THREAD_CONTEXT
}InjectionMethod_t;

typedef IGSTATUS (*memwriter_f)(PINJECTOR_GADGET, void *, size_t);
typedef IGSTATUS (*start_f)(PINJECTOR_GADGET);

struct INJECTOR_GADGET
{
    InjectionMethod_t injection_type;
    BOOL encrypted;
    memwriter_f writer;
    start_f start;
    HANDLE hProc;
    LPVOID start_addr; 
    PVOID arg;
    DWORD tid;
    DWORD pid;
};


PINJECTOR_GADGET ig_init(InjectionMethod_t type, DWORD pid, BOOLEAN xor_encrypted);
IGSTATUS gogoInjectorGadget(PINJECTOR_GADGET ig, void* buf, SIZE_T len);
IGSTATUS ig_destroy(PINJECTOR_GADGET *ig);
#endif //INJECTOR_GADGET_H
