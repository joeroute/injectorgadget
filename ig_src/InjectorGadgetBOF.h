#ifndef INJECTORGADGETBOF_H
#define INJECTORGADGETBOF_H

#include "windows.h"
#include "bofdefs.h"
WINBASEAPI HANDLE WINAPI KERNEL32$CopyMemory(LPVOID, LPVOID, SIZE_T);

WINBASEAPI HANDLE WINAPI KERNEL32$CreateRemoteThread (HANDLE hProcess, LPSECURITY_ATTRIBUTES lpThreadAttributes, SIZE_T dwStackSize, LPTHREAD_START_ROUTINE lpStartAddress, LPVOID lpParameter, DWORD dwCreationFlags, LPDWORD lpThreadId);
WINBASEAPI HANDLE WINAPI KERNEL32$CreateThread(LPSECURITY_ATTRIBUTES, SIZE_T, LPTHREAD_START_ROUTINE, LPVOID, DWORD, LPDWORD);
WINBASEAPI HANDLE WINAPI KERNEL32$GetProcessHeap (VOID);
WINBASEAPI BOOL WINAPI KERNEL32$GetThreadContext (HANDLE, LPCONTEXT);
WINBASEAPI HANDLE WINAPI KERNEL32$OpenThread (DWORD, BOOL, DWORD);
WINBASEAPI DWORD WINAPI KERNEL32$QueueUserAPC(PAPCFUNC, HANDLE, ULONG_PTR);
WINBASEAPI DWORD WINAPI KERNEL32$ResumeThread (HANDLE);
WINBASEAPI BOOL WINAPI KERNEL32$SetThreadContext (HANDLE, const CONTEXT *);
WINBASEAPI DWORD WINAPI KERNEL32$SuspendThread (HANDLE);
WINBASEAPI BOOL WINAPI KERNEL32$Thread32First(HANDLE,LPTHREADENTRY32);
WINBASEAPI BOOL WINAPI KERNEL32$Thread32Next(HANDLE,LPTHREADENTRY32);
WINBASEAPI PVOID WINAPI KERNEL32$VirtualAllocEx (HANDLE, PVOID, DWORD, DWORD, DWORD);
WINBASEAPI BOOL WINAPI KERNEL32$VirtualProtectEx (HANDLE, PVOID, DWORD, DWORD, PDWORD);
WINBASEAPI BOOL WINAPI KERNEL32$VirtualProtect (PVOID, DWORD, DWORD, PDWORD);
WINBASEAPI WINBOOL WINAPI KERNEL32$WriteProcessMemory (HANDLE hProcess, LPVOID lpBaseAddress, LPCVOID lpBuffer, SIZE_T nSize, SIZE_T *lpNumberOfBytesWritten);

//Replace normal fncalls with bof notation
#define CloseHandle KERNEL32$CloseHandle
#define CreateRemoteThread KERNEL32$CreateRemoteThread
#define CreateThread KERNEL32$CreateThread
#define CreateToolhelp32Snapshot KERNEL32$CreateToolhelp32Snapshot
#define GetProcessHeap KERNEL32$GetProcessHeap
#define GetThreadContext KERNEL32$GetThreadContext
#define HeapAlloc KERNEL32$HeapAlloc
#define HeapFree KERNEL32$HeapFree
#define OpenProcess KERNEL32$OpenProcess
#define OpenThread KERNEL32$OpenThread
#define QueueUserAPC KERNEL32$QueueUserAPC
#define ResumeThread KERNEL32$ResumeThread
#define SetThreadContext KERNEL32$SetThreadContext
#define SuspendThread KERNEL32$SuspendThread
#define Thread32First KERNEL32$Thread32First
#define Thread32Next  KERNEL32$Thread32Next
#define VirtualAlloc KERNEL32$VirtualAlloc
#define VirtualAllocEx KERNEL32$VirtualAllocEx
#define VirtualProtect KERNEL32$VirtualProtect
#define VirtualProtectEx KERNEL32$VirtualProtectEx
#define WriteProcessMemory KERNEL32$WriteProcessMemory

#include "base.c"
#include "gadgets.c"
#include "mem_writer.c"




#endif //INJECTORGADGETBOF_H
