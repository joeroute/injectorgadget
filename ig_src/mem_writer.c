#include "mem_writer.h"

IGSTATUS ig_VirtualAlloc(PINJECTOR_GADGET ig, void *buf, SIZE_T buf_size)
{
    SIZE_T bytes_written = 0;
    DWORD old_protect = 0;
    if (NULL == (ig->start_addr = VirtualAllocEx(ig->hProc, NULL, buf_size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE)))
    {
        return IG_ALLOC_ERROR;
    }
    
    if (!WriteProcessMemory(ig->hProc, ig->start_addr, buf, buf_size, &bytes_written))
    {
        return IG_WRITE_ERROR;
    }
    if (!ig->encrypted)
    {
        if(!VirtualProtectEx(ig->hProc, ig->start_addr, buf_size, PAGE_EXECUTE_READ, &old_protect))
        {
            return IG_PROTECT_ERROR;
        }
    }
    
    return IG_SUCCESS;
}

IGSTATUS ig_LocalAlloc(PINJECTOR_GADGET ig, void *buf, SIZE_T buf_size)
{
    SIZE_T bytes_written = 0;
    DWORD old_protect = 0;
    if (NULL == (ig->start_addr = VirtualAlloc(NULL, buf_size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE)))
    {
        return IG_ALLOC_ERROR;
    }
    
    CopyMemory(ig->start_addr, buf, buf_size);

    if (!ig->encrypted)
    {
        if(!VirtualProtect(ig->start_addr, buf_size, PAGE_EXECUTE_READ, &old_protect))
        {
            return IG_PROTECT_ERROR;
        }
    }
    
    return IG_SUCCESS;
}
