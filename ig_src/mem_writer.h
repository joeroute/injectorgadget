#ifndef HELPERS_H
#define HELPERS_H
#include <windows.h>
#include "InjectorGadget.h"

IGSTATUS ig_VirtualAlloc(PINJECTOR_GADGET ig, void *buf, SIZE_T buf_size);
IGSTATUS ig_LocalAlloc(PINJECTOR_GADGET ig, void *buf, SIZE_T buf_size);

#endif //HELPERS_H
